package StemString;

import DataRepo.Sense;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by abhinav.sh on 25-Aug-16.
 */
public class StemSenses {
    public void stemSenses() throws SQLException, IOException, ClassNotFoundException {

        Sense sense = new Sense ();
        StringStemmer stringStemmer = new StringStemmer ();
        ResultSet resultSet = sense.getSense ();


        while(resultSet.next ()){

            sense.setStemSense ( resultSet.getInt ( "id" ) ,stringStemmer.StemStringEnglish ( resultSet.getString ( "wordtext" ) ));

        }
        sense.closeConnection ();

    }

}
