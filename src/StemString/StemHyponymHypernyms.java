package StemString;

import DataRepo.HyponymHypernym;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by abhinav.sh on 25-Aug-16.
 */
public class StemHyponymHypernyms {

    public void stemHyponymHypernyms() throws SQLException, IOException, ClassNotFoundException {

        HyponymHypernym hyponymHypernym = new HyponymHypernym ();
        StringStemmer stringStemmer = new StringStemmer ();
        ResultSet resultSet = hyponymHypernym.getHyponymHypernym ();


        while(resultSet.next ()){

            hyponymHypernym.setHyponymHypernym ( resultSet.getInt ( "id" ),
                    stringStemmer.StemStringEnglish ( resultSet.getString ( "hyponym_text" ) ) ,
                    stringStemmer.StemStringEnglish ( resultSet.getString ( "hypernym_text" ) ) );

        }
        hyponymHypernym.closeConnection ();

    }
}
