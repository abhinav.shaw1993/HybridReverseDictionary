package StemString;


import Snowball.SnowballExt.*;
import java.io.IOException;

/**
 * Created by abhinav.sh on 25-Aug-16.
 */
public class StringStemmer {

    
    private String words[] ;
    
    public String StemStringEnglish(String stem) throws IOException, ClassNotFoundException{

        englishStemmer es = new englishStemmer ();
        words = stem.split ( " " );
        int i;
        for ( i = 0; i < words.length;i++
             ) {
            es.setCurrent ( words[i] );
            es.stem( );
            words[i] = es.getCurrent ();
        }

        return String.join(" ",words);

    }



}
