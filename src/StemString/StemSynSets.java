package StemString;

import DataRepo.SynSet;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by abhinav.sh on 25-Aug-16.
 */
public class StemSynSets {

     public void stemSynSets() throws SQLException, IOException, ClassNotFoundException {

         SynSet synSet = new SynSet ();
         StringStemmer stringStemmer = new StringStemmer ();
         ResultSet resultSet = synSet.getSynSets ();


         while(resultSet.next ()){

             synSet.setStemSynSets ( resultSet.getInt ( "id" ) ,stringStemmer.StemStringEnglish ( resultSet.getString ( "Gloss" ) ));


         }
         synSet.closeConnection ();

     }

}
