package DataRepo;

import java.io.IOException;
import java.sql.*;
import Utils.DButils;

/**
 * Created by abhinav.sh on 25-Aug-16.
 */
public class Sense {

    private Connection conn ;
    private Statement stm;


    public  Sense() throws IOException, SQLException, ClassNotFoundException {

        DButils db = new DButils();
        db.initializeDbproperties();
        conn = db.getConnection();

    }

    public ResultSet getSense() throws SQLException {
        stm = conn.createStatement();
        String sql = "select * from tblSenses";

        return stm.executeQuery ( sql );

    }

    public void setStemSense(int id,String sense) throws SQLException {

        String SpSql = "Exec map_stemmed_sense ?,?";
        PreparedStatement preparedStatement = conn.prepareStatement ( SpSql );

        preparedStatement.setQueryTimeout(2);
        preparedStatement.setInt (1, id);
        preparedStatement.setString (2, sense);
        preparedStatement.execute ();

    }

    public void closeConnection() throws SQLException {
        stm.close ();
        conn.close ();
    }

}
