package DataRepo;

import Utils.DButils;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by abhinav.sh on 01-Jan-17.
 */
public class GetRMSForWord {

    private Connection conn ;
    private PreparedStatement preparedStatement;
    private  ResultSet rs ;

    public  GetRMSForWord() throws IOException, SQLException, ClassNotFoundException {

        DButils db = new DButils();
        db.initializeDbproperties();
        conn = db.getConnection();

    }


    public Set<Integer> getRMSForWord (String word) throws SQLException
    {
        ArrayList<Integer> ReverseMappingList = new ArrayList<Integer>(1000);
        String SpSql = "Exec Get_rms_for_stemmed_word ?";
        preparedStatement = conn.prepareStatement ( SpSql );

        preparedStatement.setQueryTimeout(2);
        preparedStatement.setString (1, word);
        rs = preparedStatement.executeQuery ();

        while(rs.next ())
        {
            ReverseMappingList.add ( rs.getInt ( 1) );

        }

        Set<Integer> set  = new HashSet<> ( ReverseMappingList );


        return set;

    }

    public void closeConnection() throws SQLException {
        preparedStatement.close ();
        conn.close ();
    }

}
