package DataRepo;

import Utils.DButils;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by abhinav.sh on 21-Feb-17.
 */
public class Antonym {

    private Connection conn ;
    private  ResultSet rs ;
    private PreparedStatement preparedStatement;

    public  Antonym() throws IOException, SQLException, ClassNotFoundException {

        DButils db = new DButils();
        db.initializeDbproperties();
        conn = db.getConnection();

    }

    public List getAntonym(String word ) throws SQLException {

        String SpSql = "Exec Get_antonym_for_word ?";

        preparedStatement = conn.prepareStatement ( SpSql );
        preparedStatement.setQueryTimeout(2);
        preparedStatement.setString (1,word );

        List resultList = new ArrayList (  );

        rs =   preparedStatement.executeQuery ();

        while (rs.next ())
        {
            resultList.add ( rs.getString ( 3 ) );

        }


        return resultList;

    }

    public void closeConnection() throws SQLException {
        preparedStatement.close ();
        conn.close ();
    }



}
