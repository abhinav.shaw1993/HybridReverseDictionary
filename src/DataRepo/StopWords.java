package DataRepo;

import Utils.DButils;

import java.io.IOException;
import java.sql.*;
/**
 * Created by abhinav.sh on 30-Sep-16.
 */
public class StopWords {

    private Connection conn ;
    private Statement stm;


    public  StopWords() throws IOException, SQLException, ClassNotFoundException {

        DButils db = new DButils();
        db.initializeDbproperties();
        conn = db.getConnection();

    }

    public ResultSet getStopWords() throws SQLException {
        stm = conn.createStatement();
        String sql = "select * from tblstopwords";

        return stm.executeQuery ( sql );

    }


    public void closeConnection() throws SQLException {
        stm.close ();
        conn.close ();
    }

}
