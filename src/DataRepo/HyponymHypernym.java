package DataRepo;

import Queries.ExpandHypoNyms;
import Utils.DButils;

import java.io.IOException;
import java.sql.*;

/**
 * Created by abhinav.sh on 25-Aug-16.
 */
public class HyponymHypernym {

    private Connection conn ;
    private Statement stm;


    public  HyponymHypernym() throws IOException, SQLException, ClassNotFoundException {

        DButils db = new DButils();
        db.initializeDbproperties();
        conn = db.getConnection();
        stm = conn.createStatement();

    }

    public ResultSet getHyponymHypernym() throws SQLException {

        String sql = "select * from tblHyponymy";

        return stm.executeQuery ( sql );

    }



    public void setHyponymHypernym(int id,String hyponym, String hypernym) throws SQLException {

        String SpSql = "Exec Map_stemmed_hypernym_and_hyponym ?,?,?";
        PreparedStatement preparedStatement = conn.prepareStatement ( SpSql );

        preparedStatement.setQueryTimeout(2);
        preparedStatement.setInt (1, id);
        preparedStatement.setString (2, hyponym);
        preparedStatement.setString (3, hypernym);
        preparedStatement.execute ();

    }

    public void closeConnection() throws SQLException {

        stm.close ();
        conn.close ();

    }

}
