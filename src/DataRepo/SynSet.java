/**
 * Created by abhinav.sh on 24-Aug-16.
 */
package DataRepo;

import java.io.IOException;
import java.sql.*;
import Utils.DButils;

public class SynSet {

    private Connection conn ;
    private Statement stm;


    public  SynSet() throws IOException, SQLException, ClassNotFoundException {

        DButils db = new DButils();
        db.initializeDbproperties();
        conn = db.getConnection();

    }

    public ResultSet getSynSets() throws SQLException {
        stm = conn.createStatement();
        String sql = "select * from tblSynsets";

        return stm.executeQuery ( sql );

    }

    public void setStemSynSets(int id,String synSet) throws SQLException {

        String SpSql = "Exec map_stemmed_syn_set ?,?";
        PreparedStatement preparedStatement = conn.prepareStatement ( SpSql );

        preparedStatement.setQueryTimeout(2);
        preparedStatement.setInt (1, id);
        preparedStatement.setString (2, synSet);
        preparedStatement.execute ();

    }

    public void closeConnection() throws SQLException {
        stm.close ();
        conn.close ();
    }

}
