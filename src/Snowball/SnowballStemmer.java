
package Snowball;

public abstract class SnowballStemmer extends SnowballProgram {
    public abstract boolean stem();
};
