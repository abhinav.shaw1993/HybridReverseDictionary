package Parser;


/**
 * Created by abhinav.sh on 29-Sep-16.
 *
 * Stack Object For The Parser. Probably Will Have A Different DataStructure.
 *
 */
public class OperatorStack {

    private int maxSize ;
    private String[] stackArray;
    private int top;

    public OperatorStack (int s)
    {
        maxSize = s;

        stackArray = new String[maxSize];
        top=-1;
    }

    public void push(String j) {
        stackArray[++top] = j;
    }

    public String pop() {
        return stackArray[top--];
    }

    public String peek() {
        return stackArray[top];
    }

    public boolean isEmpty() {
        return (top == -1);
    }

    public boolean isFull() {
        return (top == maxSize - 1);
    }


}
