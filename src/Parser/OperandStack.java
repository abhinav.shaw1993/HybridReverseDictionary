package Parser;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Arrays;

/**
 * Created by abhinav.sh on 29-Sep-16.
 */
public class OperandStack {

    private int maxSize ;
    private List<Map<String, Set<Integer> >> stackList;
    private int top;

 /*   public OperandStack (int s)
    {
        maxSize = s;
        stackList = Arrays.asList(new  Map<String, Set<Integer> >[maxSize]);
        top=-1;
    }*/

    public void push(Map<String, Set<Integer> > map) {
        stackList.add(++top, map);
    }

    public Map<String, Set<Integer>> pop() {
        return stackList.remove(top--);
    }

    public Map<String, Set<Integer>>peek() {
        return stackList.get(top);
    }

    public boolean isEmpty() {
        return (top == -1);
    }

    public boolean isFull() {
        return (top == maxSize - 1);
    }


}
