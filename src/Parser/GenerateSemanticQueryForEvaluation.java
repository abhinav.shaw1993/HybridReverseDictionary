package Parser;


import DataRepo.Antonym;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by abhinav.sh on 02-Jan-17.
 */
public class GenerateSemanticQueryForEvaluation {

    private String query;
    private List queryList = new ArrayList() ;
    private String expression = "" ;
    private List<String> ecpressionList = new ArrayList<String>(  );

    private final List negationWords = new ArrayList<> (   );


    private void initNegationWords (){

        negationWords.add ("cannot");
        negationWords.add ("not");
        negationWords.add ("can't");
        negationWords.add ("shan't");

    }

    public void setQuery(String q)
    {
        query = q ;

        initNegationWords ();

    }

    private void negateQuery () throws SQLException, IOException, ClassNotFoundException {

        Antonym antonym = new Antonym ();

        StringTokenizer tokens  = new StringTokenizer ( query, " " );

        List resultList = new ArrayList ();

        List antonymsList ;

        while (tokens.hasMoreTokens ())
        {
            String token = tokens.nextToken ().toLowerCase ();

            if ( negationWords.contains( token))
            {
                antonymsList =  antonym.getAntonym ( tokens.nextToken ().toLowerCase () );

                resultList.add ( new ArrayList<> ( antonymsList ) );

            }

            else
            {
                List<String> temp = new ArrayList<> (  );
                temp.add ( token );
                resultList.add ( temp );

            }
        }

        queryList = resultList;

    }

    private String addParenthesesToQuery(String q)
    {
        String[] Array = q.split ( " " );

        for (int i = 0; i < Array.length; i++) {
             Array[i] = "(" + Array[i] + ")" ;
        }

        return String.join ( " " ,Array );

    }

    private String surroundQueryWithParentheses (String q)
    {

        return     "( " + q + " )"  ;

    }

    private  String addUnionsToQuery(String q)
    {
        String[] Array = q.split ( " " );

        return String.join ( " UNION " ,Array );


    }

    private String addIntersectionsToQuery( ArrayList<String> list)
    {
        String [] s = list.toArray (new String [0]);

        System.out.println (String.join ( " INTERSECTION " ,s ));
        return String.join ( " INTERSECTION " ,s );

    }

    private String computeExpression()
    {

        if ( queryList.size () == 0)
            return "Invalid Query";

        else
        {
            Iterator queryListIterator = queryList.iterator ();


            while ( queryListIterator.hasNext () )
            {

                List tempList ;
                tempList = (List) queryListIterator.next ();
                String partialExpression;

                if (tempList.size () > 1) {

                    partialExpression = String.join ( " ",(String [])tempList.toArray(new String [0]));
                    partialExpression = addUnionsToQuery ( partialExpression );

                    partialExpression = surroundQueryWithParentheses ( partialExpression );
                }
                else
                partialExpression = Arrays.toString(tempList.toArray());

                ecpressionList.add ( partialExpression );

            }

            expression = addIntersectionsToQuery (new   ArrayList( ecpressionList) );

            return "Success";

        }

    }

    public String getExpression () throws SQLException, IOException, ClassNotFoundException {

        negateQuery();
        System.out.println ( computeExpression ());
        return expression;

    }

}
