/**
 * Created by abhinav.sh on 24-Aug-16.
 */
package Utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DButils {

    private String connectionString;
    private String driver;


        public void initializeDbproperties() throws IOException
        {
            InputStream inputStream;
            Properties dbProp = new Properties();
            String propFileName = "DbProperties.properties";


            inputStream  = getClass().getClassLoader().getResourceAsStream(propFileName);
            if(inputStream != null){
                dbProp.load(inputStream);
            }else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }

            connectionString=dbProp.getProperty("connection_string");
            driver=dbProp.getProperty("driver");
        }

        public Connection getConnection () throws SQLException , ClassNotFoundException
        {
            Class.forName(driver);

            return DriverManager.getConnection(connectionString);

        }



}
