package Queries;

import Utils.DButils;

import java.io.IOException;
import java.sql.*;

/**
 * Created by abhinav.sh on 06-Oct-16.
 */
public class ExpandHyperNyms {

    private Connection conn ;
    private Statement stm;

    public  ExpandHyperNyms() throws IOException, SQLException, ClassNotFoundException {

        DButils db = new DButils();
        db.initializeDbproperties();
        conn = db.getConnection();
        stm = conn.createStatement();

    }

    public ResultSet expandHyperNym (String word) throws SQLException {

        String SpSql = "Exec Get_hypernym_for_word ?";
        PreparedStatement preparedStatement = conn.prepareStatement ( SpSql );

        preparedStatement.setQueryTimeout(2);
        preparedStatement.setString (1, word);
        return preparedStatement.executeQuery ();

    }
}
