package Queries;

import Criteria.IsOperand;
import Criteria.IsOperator;
import Parser.OperandStack;
import Sets.SetOperations;
import Parser.OperatorStack;
import java.io.IOException;
import java.util.*;

/**
 * Created by abhinav.sh on 01-Oct-16.
 */
public class ExecuteQuery {

    private OperatorStack operatorStack;

    private OperandStack operandStack  ;

    private IsOperand isOperand = new IsOperand ();

    private IsOperator isOperator = new IsOperator ();

    public void computeQuery(String expression , Map<String, Set<Integer>> querySet) throws  IOException
    {

        StringTokenizer st = new StringTokenizer ( expression , " ");
        operatorStack = new OperatorStack( st.countTokens ());
        operandStack = new OperandStack ( );

        while (st.hasMoreElements())
        {

            String token = st.nextToken ();

            if (isOperand.isOperand ( token )) {
                operateIfOperand ( token, querySet );
            }

           else  if (isOperator.isLeftParenthesis ( token )) {
                operateIfLeftParenthesis ( token );
            }

           else  if (isOperator.isRightParenthesis ( token )) {
                operateIfRightParenthesis ( );
            }

            else  if (isOperator.isOperator ( token ) && !isOperator.isRightParenthesis ( token )
                        && !isOperator.isLeftParenthesis ( token ))
            {
                operateIfOperator ( token );
            }


        }
        while (!operatorStack.isEmpty ())
        {

            if ( isOperator.isUnion ( operatorStack.peek () ))
            {
                operatorStack.pop ();
                operateIfUnion ();
            }

            else if ( isOperator.isIntersection ( operatorStack.peek () ))
            {
                operatorStack.pop ();
                operateIfIntersection ();
            }

            else if  ( isOperator.isLeftParenthesis ( operatorStack.peek () ) )
                System.out.println ("Discarded Value :" + operatorStack.pop ());

        }

    }

    public void operateIfIntersection(  ) throws  IOException
    {
        Map<String, Set<Integer>> tempMap = new HashMap<> ();
        Set<Integer> resultSet ;
        String resultKey ;
        Map<String, Set<Integer>> tempOperator1 = new HashMap<> ();
        Map<String, Set<Integer>> tempOperator2 = new HashMap<> ();

        tempOperator1.clear ();
        tempOperator2.clear ();

        tempOperator1 =   operandStack.pop ();

        tempOperator2 =   operandStack.pop ();

        Map.Entry<String,Set<Integer>> operatorEntry1=tempOperator1.entrySet().iterator().next();
        String operatorKey1= operatorEntry1.getKey();
        Set<Integer> operatorValue1=operatorEntry1.getValue();

        Map.Entry<String,Set<Integer>> operatorEntry2=tempOperator2.entrySet().iterator().next();
        String operatorKey2= operatorEntry2.getKey();
        Set<Integer> operatorValue2=operatorEntry2.getValue();

        SetOperations setOperations = new SetOperations ();
        resultSet = setOperations.intersection ( operatorValue1,operatorValue2 );

        resultKey = operatorKey1 + " INTERSECTION " + operatorKey2;

        tempMap.clear ();

        tempMap.put ( resultKey,resultSet );

        operandStack.push  (tempMap);

    }

    public void operateIfUnion (  ) throws  IOException
    {
        Map<String, Set<Integer>> tempMap = new HashMap<> ();
        Set<Integer> resultSet ;
        String resultKey ;
        Map<String, Set<Integer>> tempOperand1 = new HashMap<> ();
        Map<String, Set<Integer>> tempOperand2 = new HashMap<> ();

        tempOperand1.clear ();
        tempOperand2.clear ();

        tempOperand1 =   operandStack.pop ();

        tempOperand2 =   operandStack.pop ();

        Map.Entry<String,Set<Integer>> operatorEntry1=tempOperand1.entrySet().iterator().next();
        String operatorKey1= operatorEntry1.getKey();
        Set<Integer> operatorValue1=operatorEntry1.getValue();

        Map.Entry<String,Set<Integer>> operatorEntry2=tempOperand2.entrySet().iterator().next();
        String operatorKey2= operatorEntry2.getKey();
        Set<Integer> operatorValue2=operatorEntry2.getValue();

        SetOperations setOperations = new SetOperations ();
        resultSet = setOperations.union ( operatorValue1,operatorValue2 );

        resultKey = operatorKey1 + " UNION " + operatorKey2;

        tempMap.clear ();

        tempMap.put ( resultKey,resultSet );

        operandStack.push  (tempMap);

    }

    public void operateIfOperand (  String token ,Map <String ,Set<Integer>> querySet )
    {
        Map<String, Set<Integer>> tempMap = new HashMap<> ();
        tempMap.clear ();
        tempMap.putIfAbsent(token,querySet.get ( token ) );

        operandStack.push (tempMap);


    }

    public void operateIfLeftParenthesis ( String token   ){

        operatorStack.push ( token );

    }

    public void operateIfRightParenthesis (   ) throws
    IOException{

        while (   !operatorStack.peek ().equals ( "(" ))
        {

            if (isOperator.isUnion ( operatorStack.peek () ))
            {
                operatorStack.pop ();
                operateIfUnion (); }

            else if (isOperator.isIntersection ( operatorStack.peek () ))
            {
                operatorStack.pop ();
                operateIfIntersection ();}

        }

        if (isOperator.isLeftParenthesis ( operatorStack.peek () ))
            System.out.println ("Discarded Value :" + operatorStack.pop ());

    }


    public void operateIfOperator (  String thisOperator  ) throws
            IOException{

        while (   !operatorStack.isEmpty ()  && isTopOperatorPrecedenceGreaterOrEqualThanThisOperator(thisOperator)) ;
        {

            if( thisOperator.equals ( "UNION" ) ){
                operatorStack.pop ();
                operateIfUnion ();
            }

            else if( thisOperator.equals ( "INTERSECTION" ) ) {
                operatorStack.pop ();
                operateIfIntersection ();
            }


        }

        operatorStack.push ( thisOperator );

    }


    public boolean isTopOperatorPrecedenceGreaterOrEqualThanThisOperator( String thisOperator){

        String topOperator = operatorStack.peek ();

        if (   topOperator.toUpperCase () == thisOperator.toUpperCase () )
            return true;

        else if (   topOperator.toUpperCase () == "INTERSECTION" && thisOperator.toUpperCase () == "UNION" )
            return true;

        else if (   topOperator.toUpperCase () == "UNION" && thisOperator.toUpperCase () == "INTERSECTION" )
            return false;

        return false;
    }

    public Map<String, Set<Integer>> getResultSetsToBeRanked ()
    {

        return operandStack.pop();

    }

}
