package Queries;

import DataRepo.GetRMSForWord;
import StemString.StringStemmer;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by abhinav.sh on 01-Oct-16.
 */
public class GenerateQuery {
    private String Query;
    private String Terms[];



    /** Set Query **/
    public void setQuery(String s) {

        Query = s;

    }

    /** Removes Stop Words From Query and Stems The Input**/
    public String EnrichQuery() throws IOException ,SQLException, ClassNotFoundException{

        Query = Query.toLowerCase ();
        RemoveStopWords rsm = new RemoveStopWords ();
        Query = rsm.GetRefinedStringByRemovingStopWordsAndStemming ( Query );

//        StringStemmer ss = new StringStemmer ();
//        Query = ss.StemStringEnglish ( Query );

        GetDeserializedTerms Gt = new GetDeserializedTerms ();
        Terms = Gt.GetDeserializedString ( Query );

        return Query;
    }

    /** Return HashSet From The Terms That Were Previously Set**/
    public Map<String, Set<Integer> > GetSetsFromQuery (  ) throws SQLException, IOException, ClassNotFoundException {

        Map<String, Set<Integer>> QueryTerm = new HashMap<> ();
        GetRMSForWord RMS = new GetRMSForWord ();

        for (String q : Terms)
        {

            QueryTerm.putIfAbsent ( q, RMS.getRMSForWord ( q ) );

        }

        RMS.closeConnection ();
        return QueryTerm;



    }

}
