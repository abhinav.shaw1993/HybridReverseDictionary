package Queries;

import DataRepo.StopWords;
import org.apache.commons.lang.ArrayUtils;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import StemString.StringStemmer;

/**
 * Created by abhinav.sh on 30-Sep-16.
 */
public class RemoveStopWords {

    public String GetRefinedStringByRemovingStopWordsAndStemming( String UserInput)throws SQLException,ClassNotFoundException,IOException
    {

        String[] terms ;

        terms = UserInput.split ( " " );
        StopWords stopWords = new StopWords ();

        ResultSet rs = stopWords.getStopWords ();
        String CurremtStopWord ;


        while (rs.next ())
        {
            CurremtStopWord =rs.getString ( "StopWord" );

            if(Arrays.asList(terms).contains(CurremtStopWord));
            terms = (String[]) ArrayUtils.removeElement(terms,CurremtStopWord);
        }

        StringStemmer strstm = new StringStemmer ();

        stopWords.closeConnection ();

//        stopWords.closeConnection ();
        return strstm.StemStringEnglish ( String.join ( " ",terms ));


    }

}
