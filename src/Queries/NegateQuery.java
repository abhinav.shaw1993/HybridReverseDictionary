package Queries;


import java.util.ArrayList;
import java.util.Arrays;


/**
 * Created by abhinav.sh on 01-Jan-17.
 */
public class NegateQuery {

    private String Query;

    public String GetNegatedQuery (String Query)
    {
        String[] Terms ;
        Terms = Query.split ( " " );

        String  NegationWords[] = {"not","cannot"};

        ArrayList<String> negationWords = new ArrayList<>(25);

        negationWords.addAll ( Arrays.asList (NegationWords));

        return String.join ( " ", negationWords.toArray ().toString ());
    }


}
