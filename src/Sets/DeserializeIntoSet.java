package Sets;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by abhinav.sh on 02-Jan-17.
 */
public class DeserializeIntoSet {

    private Set<Integer> set ;

    public Set<Integer> DeserializeIntoSet ( ArrayList list) {

        Set<Integer> set = new HashSet<Integer> (list);

        return set;

    }

}
