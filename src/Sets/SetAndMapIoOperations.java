package Sets;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;
import java.util.Map;

/**
 * Created by abhinav.sh on 08-Apr-17.
 */
public class SetAndMapIoOperations {

    public void printIntegerSet (Set <Integer> s)throws IOException
    {

        for (Integer i :  s) {
            System.out.println(s);
        }
        System.out.println ();


    }

    public void printHashMap ( Map < String , Set <Integer> > map)throws IOException
    {

        Iterator iterator = map.keySet().iterator();

        while (iterator.hasNext()) {
            String key = iterator.next().toString();
            Set<Integer > set = map.get(key);

            printIntegerSet(set);

        }

    }
}
