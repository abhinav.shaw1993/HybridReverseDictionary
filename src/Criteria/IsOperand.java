package Criteria;

import java.io.IOException;

/**
 * Created by abhinav.sh on 06-Feb-17.
 */
public class IsOperand {

    public boolean isOperand(String s) throws IOException

    {
        if (s.length () < 3)
            return false;

        if (s.toUpperCase ().equals("UNION"))
            return false;

        else if  (s.toUpperCase ().equals("INTERSECTION"))
            return false;

        else if  (s.toUpperCase ().equals("("))
            return false;

        else if  (s.toUpperCase ().equals(")"))
            return false;

        return true;
    }


}
