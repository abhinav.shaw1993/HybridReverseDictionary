package Criteria;

import java.io.*;


/**
 * Created by abhinav.sh on 06-Feb-17.
 */
public class IsOperator {

    public boolean isOperator(String s) throws IOException

    {
        if (s.toUpperCase ().equals("UNION"))
            return true;

        else if  (s.toUpperCase ().equals("INTERSECTION"))
            return true;

        else if  (s.toUpperCase ().equals("("))
            return true;

        else if  (s.toUpperCase ().equals(")"))
            return true;

        return false;
    }

    public boolean isLeftParenthesis (String s ) throws IOException
    {
        if (s.toUpperCase ().equals (  "("  )  )
            return true;


        return false;
    }

    public boolean isRightParenthesis (String s ) throws IOException
    {
        if (s.toUpperCase ().equals (  ")"  )  )
            return true;

        return false;
    }

    public boolean isUnion (String s ) throws IOException
    {
        if (s.toUpperCase ().equals (  "UNION"  )  )
            return true;

        return false;
    }

    public boolean isIntersection (String s ) throws IOException
    {
        if (s.toUpperCase ().equals (  "INTERSECTION"  )  )
            return true;

        return false;
    }

}
